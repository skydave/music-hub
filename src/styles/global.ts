import { createGlobalStyle } from "styled-components";
import theme from './theme';
const GlobalStyle = createGlobalStyle`
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,900|Roboto');
    body {
        font-family: 'Roboto', sans-serif;
        background: ${theme.colors.grey.ultraLight}
    }
`;
export { GlobalStyle };
