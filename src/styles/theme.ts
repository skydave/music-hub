import { DefaultTheme } from "styled-components";



const theme: DefaultTheme = {
  colors: {
    grey: {
      ultraLight: "#f9f9fa",
      light: "#dee1e3",
      medium: "#adb4b9",
      dark: "#7f8b93",
    },
  },
  font: {
    weight: {
      normal: "500",
      medium: "600",
      bold: "700",
    },
    size: {
      tiny: `10px`,
      small: `12px`,
      normal: "14px",
      medium: "16px",
      large: `20px`,
      extraLarge: `24px`,
    },
    family: {
      default: "",
    },
  },
  spacing: {
    tiny: "10px",
    small: "12px",
    medium: "14px",
    large: "18px",
  },
};

export default theme;
