import React from "react";
import AlbumList from "../components/AlbumList/AlbumList";
import PageLayout from "../components/PageLayout/PageLayout";
import SearchBar from "../components/SearchBar/SearchBar";

const SearchResultPage = () => {
  return (
    <PageLayout>
      <SearchBar />
      <AlbumList />
    </PageLayout>
  );
};

export default SearchResultPage;
