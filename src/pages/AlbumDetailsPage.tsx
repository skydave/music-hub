import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import styled from "styled-components";
import fetchAlbumDetails from "../api/fetchAlbumDetails";
import { AlbumDetails } from "../components/AlbumInfo/AlbumDetails.type";
import AlbumInfo from "../components/AlbumInfo/AlbumInfo";
import PageLayout from "../components/PageLayout/PageLayout";
import StyledLink from "../components/StyledLink/StyledLink";

const Back = styled.div`
  width: 100%;
  display: flex;
  align-items: flex-start;
  color: ${({ theme }) => theme.colors.grey.dark};
  font-weight: ${({ theme }) => theme.font.weight.medium};
  font-size: ${({ theme }) => theme.font.size.large};
`;

const AlbumDetailsPage = () => {
  const { id } = useParams();
  const [albumDetails, setAlbumDetails] = useState<AlbumDetails | undefined>();
  useEffect(() => {
    fetchAlbumDetails(id).then((data: AlbumDetails) => setAlbumDetails(data));
  }, [id]);

  return (
    <PageLayout>
      <Back>
        <StyledLink to="/searchResultPage">Back</StyledLink>
      </Back>
      {albumDetails && <AlbumInfo albumDetails={albumDetails} />}
    </PageLayout>
  );
};

export default AlbumDetailsPage;
