import React, { useEffect, useState } from "react";
import styled from "styled-components";
import PageLayout from "../components/PageLayout/PageLayout";

const Loader = styled.div`
  color: ${({ theme }) => theme.colors.grey.dark};
  font-weight: ${({ theme }) => theme.font.weight.medium};
  font-size: ${({ theme }) => theme.font.size.large};
  padding: 50px;
`;

const LoginMessage = styled.div`
  color: ${({ theme }) => theme.colors.grey.dark};
  font-weight: ${({ theme }) => theme.font.weight.medium};
  font-size: ${({ theme }) => theme.font.size.large};
  padding: 50px;
`;

const LoginButton = styled.button``;

const HomePage = () => {
  const [status, setStatus] = useState<
    "authorized" | "unauthorized" | "checking" | "error"
  >("checking");
  useEffect(() => {
    console.log("SearchResultPage");
    fetch("/v1/me", { method: "GET" }).then((resp: any) => {
      if (resp.status === 401) {
        setStatus("unauthorized");
        return;
      }
      if (resp.status === 200) {
        setStatus("authorized");
        window.location.replace("/searchResultPage");
        return;
      }
      setStatus("error");
    });
  }, [status]);

  return (
    <PageLayout>
      {status === "error" && <Loader>Something went wrong ...</Loader>}
      {status === "checking" && <Loader>Checking permissions ...</Loader>}
      {status === "unauthorized" && (
        <>
          <LoginMessage>Please login using Spotify credentials</LoginMessage>
          <LoginButton onClick={() => window.location.replace("/login")}>
            Login
          </LoginButton>
        </>
      )}
    </PageLayout>
  );
};

export default HomePage;
