import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import HomePage from "./pages/HomePage";
import SearchResultPage from "./pages/SearchResultPage";
import AlbumDetailsPage from "./pages/AlbumDetailsPage";
import theme from "./styles/theme";
import { SearchContextProvider } from "./components/SearchContext/SearchContext";
function App() {
  return (
    <ThemeProvider theme={theme}>
      <SearchContextProvider>
        <Router>
          <Switch>
            <Route exact path="/">
              <HomePage />
            </Route>
            <Route exact path="/searchResultPage">
              <SearchResultPage />
            </Route>
            <Route path="/albumDetails/:id">
              <AlbumDetailsPage />
            </Route>
          </Switch>
        </Router>
      </SearchContextProvider>
    </ThemeProvider>
  );
}

export default App;
