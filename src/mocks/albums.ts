import { Album } from "../components/AlbumListItem/Album.type";
const albums: Album[] = [
  {
    artist: "Sonic Youth",
    title: "Kool Thing",
    id: "album1",
    cover: "https://upload.wikimedia.org/wikipedia/en/b/b2/Sonic_Youth_Goo.jpg",
  },

  {
    artist: "Sonic Youth",
    title: "Scooter and Jinx",
    id: "album2",
    cover: "https://upload.wikimedia.org/wikipedia/en/b/b2/Sonic_Youth_Goo.jpg",
  },

  {
    artist: "Sonic Youth",
    title: "Disappearer",
    id: "album1",
    cover: "https://upload.wikimedia.org/wikipedia/en/b/b2/Sonic_Youth_Goo.jpg",
  },
];

export default albums;
