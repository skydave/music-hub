const { createProxyMiddleware } = require("http-proxy-middleware");
const fetch = require("node-fetch");
const cookieParser = require('cookie-parser');

const client_secret = "926ca9d7abcc4f2b9924157c0c202558";
const client_id = "b3dae1eaf0ac4841954c54ed5d071793";

const apiToken = async (code) =>
  fetch("https://accounts.spotify.com/api/token", {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization:
        "Basic " +
        Buffer.from(client_id + ":" + client_secret).toString("base64"),
    },
    body: new URLSearchParams(
      `code=${code}&redirect_uri=${encodeURIComponent(
        "http://localhost:3000/authenticate/"
      )}&grant_type=authorization_code`
    ),
  }).then((response) => response.json());

module.exports = function (app) {
  app.use(cookieParser());

  app.use(
    "/v1",
    createProxyMiddleware({
      target: "https://api.spotify.com/",
      changeOrigin: true,
      logLevel: "debug",
      onProxyReq: (proxyReq, req, res) => {
        proxyReq.setHeader("Authorization", `Bearer ${req.cookies.access_token}`);
      }
    })
  );

  app.get("/login", function (req, res) {
    res.redirect(
      "https://accounts.spotify.com/authorize" +
        "?response_type=code" +
        "&client_id=" +
        client_id +
        "&redirect_uri=" +
        encodeURIComponent("http://localhost:3000/authenticate/")
    );
  });
  
  app.get("/authenticate/", async function (req, res) {
    const authentication = await apiToken(req.query.code);
    res.cookie("access_token", authentication.access_token, {
      maxAge: authentication.expires_in * 1000,
    }); // consider to keep it in session
    res.redirect("http://localhost:3000/");
  });
};
