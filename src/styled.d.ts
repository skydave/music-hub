import "styled-components";

declare module "styled-components" {
  interface Size {
    tiny?: string;
    small?: string;
    normal?: string;
    medium?: string;
    large?: string;
    extraLarge?: string;
  }
  
  export interface DefaultTheme {
    colors: {
      grey: {
        ultraLight: string;
        light: string;
        medium: string;
        dark: string;
      };
    };
    font: {
      weight: {
        normal: string;
        medium: string;
        bold: string;
      };
      size: Size;
      family: {
        default: "";
      };
    };
    spacing: Size;
  }
}
