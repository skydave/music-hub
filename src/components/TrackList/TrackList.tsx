import React from "react";
import { Track } from "../AlbumInfo/AlbumDetails.type";
import styled from "styled-components";

const TrackInfo = styled.div`
  display: flex;
  flex-flow: row;
`;

const TrackList = ({ trackList }: { trackList: Track[] }) => (
  <>
    {trackList.map((track, index) => (
      <TrackInfo>
        <>{++index}.</>
        <>{track.name}</>
      </TrackInfo>
    ))}
  </>
);

export default TrackList;
