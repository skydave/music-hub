import React, { useContext, useState } from "react";
import styled from "styled-components";
import { SearchContext } from "../SearchContext/SearchContext";
const Title = styled.div`
  color: ${({ theme }) => theme.colors.grey.dark};
  font-weight: ${({ theme }) => theme.font.weight.bold};
  font-size: ${({ theme }) => theme.font.size.extraLarge};
  padding: ${({ theme }) => theme.spacing.medium};
  text-align: center;
`;
const SearchInput = styled.input`
  -webkit-appearance: none;
  border-color: ${({ theme }) => theme.colors.grey.light};
  height: 20px;
  width: 100%;
`;
const Container = styled.div`
  width: 400px;
`;

const SearchBar = () => {
  const { searchQuery, setSearchQuery } = useContext(SearchContext);
  const [inputValue, setInputValue] = useState<string | undefined>(searchQuery);
  return (
    <Container>
      <Title>Search an album</Title>
      <SearchInput
        value={inputValue}
        onChange={(e) => setInputValue(e.target.value)}
        onKeyDown={(e) => {
          if (e.key === "Enter") {
            setSearchQuery(inputValue);
          }
        }}
      />
    </Container>
  );
};

export default SearchBar;
