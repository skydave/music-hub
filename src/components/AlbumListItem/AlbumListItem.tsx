import React from "react";
import styled from "styled-components";
import { Album } from "./Album.type";

const AlbumContainer = styled.div`
  border-color: ${({ theme }) => theme.colors.grey.medium};
  border-style: solid;
  border-width: 1px;
  display: flex;
  width: 100%;
  margin-top: 20px;
`;
const ImageContainer = styled.div`
  display: flex;
`;
const Block = styled.div`
  display: flex;
`;

const Image = styled.img`
  object-fit: contain;
  height: 150px;
`;

const AlbumInfo = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: space-evenly;
  color: ${({ theme }) => theme.colors.grey.dark};
  font-weight: ${({ theme }) => theme.font.weight.medium};
  font-size: ${({ theme }) => theme.font.size.large};
  width: 100%;
  align-items: center;
`;

const AlbumListItem = ({ album }: { album: Album }) => (
  <AlbumContainer>
    <ImageContainer>
      <Image src={album.cover} alt={album.artist} />
    </ImageContainer>
    <AlbumInfo>
      <Block>{album.artist}</Block>
      <Block>{album.title}</Block>
    </AlbumInfo>
  </AlbumContainer>
);

export default AlbumListItem;
