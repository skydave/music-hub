export interface Album {
  id: string;
  artist: string;
  title: string;
  cover: string;
}
