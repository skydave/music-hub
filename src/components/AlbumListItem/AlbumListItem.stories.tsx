import React from "react";
import { storiesOf } from "@storybook/react";
import AlbumListItem from "./AlbumListItem";

storiesOf("ALbum List Item", module).add("default view", () => (
  <AlbumListItem
    album={{
      artist: "Sonic Youth",
      title: "Kool Thing",
      id: "album1",
      cover:
        "https://upload.wikimedia.org/wikipedia/en/b/b2/Sonic_Youth_Goo.jpg",
    }}
  />
));
