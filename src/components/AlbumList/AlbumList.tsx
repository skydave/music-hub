import React, { useContext } from "react";
import styled from "styled-components";
import { Album } from "../AlbumListItem/Album.type";
import AlbumListItem from "../AlbumListItem/AlbumListItem";
import { SearchContext } from "../SearchContext/SearchContext";
import StyledLink from "../StyledLink/StyledLink";
const ItemContainer = styled.div`
  padding: ${({ theme }) => theme.spacing.large};
  width: 100%;
`;

const AlbumList = () => {
  const { searchData } = useContext(SearchContext);

  return (
    <>
      {searchData.map((value: Album) => (
        <ItemContainer key={value.id}>
          <StyledLink to={`/albumDetails/${value.id}`}>
            <AlbumListItem album={value} />
          </StyledLink>
        </ItemContainer>
      ))}
    </>
  );
};

export default AlbumList;
