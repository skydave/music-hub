import styled from "styled-components";

const PageLayout = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  margin: 0 20%
`;

export default PageLayout;
