import { Album } from "../AlbumListItem/Album.type";

export interface Track {
  id: string;
  name: string;
}

export interface AlbumDetails {
  tracksList: Track[];
  album: Album;
}
