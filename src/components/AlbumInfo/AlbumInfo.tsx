import React from "react";
import { AlbumDetails } from "./AlbumDetails.type";
import AlbumListItem from "../AlbumListItem/AlbumListItem";
import TrackList from "../TrackList/TrackList";

const AlbumInfo = ({ albumDetails }: { albumDetails: AlbumDetails }) => (
  <>
    <AlbumListItem album={albumDetails.album}></AlbumListItem>
    <TrackList trackList={albumDetails.tracksList} />
  </>
);

export default AlbumInfo;
