import React, {
  createContext,
  useState,
  Dispatch,
  SetStateAction,
  useEffect,
} from "react";
import { Album } from "../AlbumListItem/Album.type";
import fetchAlbums from "../../api/fetchAlbums";

interface SearchContextType {
  searchQuery: string | undefined;
  searchData: Album[];
  setSearchQuery: Dispatch<SetStateAction<string | undefined>>;
  setSearchData: Dispatch<SetStateAction<Album[]>>;
}
export const SearchContext = createContext<SearchContextType>({
  searchQuery: "",
  searchData: [],
  setSearchQuery: () => {},
  setSearchData: () => {},
} as SearchContextType);

export const SearchContextProvider = (props: any) => {
  const [searchQuery, setSearchQuery] = useState<string | undefined>();
  const [searchData, setSearchData] = useState<Album[]>([]);

  useEffect(() => {
    if (searchQuery && searchQuery.length > 0) {
      fetchAlbums(searchQuery).then((data) => {
        setSearchData(data);
      });
    }
  }, [searchQuery, setSearchData]);

  return (
    <SearchContext.Provider
      value={{ searchQuery, setSearchQuery, searchData, setSearchData }}
    >
      {props.children}
    </SearchContext.Provider>
  );
};
