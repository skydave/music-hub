import { Album } from "../components/AlbumListItem/Album.type";

const fetchAlbums = async (searchQuery: string): Promise<Album[]> =>
  fetch(`/v1/search?q=${encodeURIComponent(searchQuery)}&type=album`)
    .then((response) => response.json())
    .then((data) =>
      data.albums?.items.map(
        (item: any): Album => ({
          artist: item.artists[0].name,
          id: item.id,
          cover: item.images[0].url,
          title: item.name,
        })
      )
    );

export default fetchAlbums;
