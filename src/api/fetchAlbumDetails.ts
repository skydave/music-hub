import { Track } from "./../components/AlbumInfo/AlbumDetails.type";
import { AlbumDetails } from "../components/AlbumInfo/AlbumDetails.type";
const fetchAlbumDetails = async (id: string): Promise<AlbumDetails> =>
  fetch(`/v1/albums/${id}`)
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      return {
        album: {
          artist: data.artists[0].name,
          id: data.id,
          cover: data.images[0].url,
          title: data.name,
        },
        tracksList: data.tracks.items.map(({ name, id }: Track) => ({
          name,
          id,
        })),
      };
    });

export default fetchAlbumDetails;
