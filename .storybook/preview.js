import React from "react";
import { addDecorator } from "@storybook/react";
import { GlobalStyle } from "../src/styles/global";
import theme from "../src/styles/theme";
import { ThemeProvider } from "styled-components";
addDecorator((story) => (
  <>
    <GlobalStyle />
    {story()}
  </>
));

addDecorator((story) => (
  <>
    <ThemeProvider theme={theme}>{story()}</ThemeProvider>
  </>
));
